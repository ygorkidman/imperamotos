<?php

class pecas
{

    private $id;
    private $descri;
    private $preco;
    private $foto;

    /**
     * @return mixed
     */
    public function getDescri()
    {
        return $this->descri;
    }

    /**
     * @param mixed $descri
     */
    public function setDescri($descri)
    {
        $this->descri = $descri;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    }


}


?>