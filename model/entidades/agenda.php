<?php

class agenda
{


    private $datarev;
    private $descricao;
    private $hora;

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora)
    {
        $this->hora = $hora;
    }


    /**
     * @return mixed
     */
    public function getDatarev()
    {
        return $this->datarev;
    }

    /**
     * @param mixed $datarev
     * @return agenda
     */
    public function setDatarev($datarev)
    {
        $this->datarev = $datarev;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }


}