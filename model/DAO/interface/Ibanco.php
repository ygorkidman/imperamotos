<?php
require_once "./../../../views/users/register/cadastro.php";
require_once "./../usuarioDAO.php";

interface Ibanco{

    public function novo();
    public function alterar();
    public function excluir();
    public function listar();

}