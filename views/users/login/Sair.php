<?php

require "./../../../main.php";

?>

<html>
<head>
    <link rel="stylesheet" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../assets/css/style.css">
</head>
<body>

<?php
session_start();


if (empty($_SESSION['login']) == true) {
    header("location: localhost:1234/home.php");
} else {
    unset($_SESSION['login']);
    echo "<h1><center>Deslogado com sucesso! <a href='index.php'><br>Fazer Login </a></center></h1>";
}
?>

</body>

</html>