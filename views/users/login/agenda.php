<?php
include "./../../../main.php";
?>

<html>
<head>
    <link rel="stylesheet" href="../../../assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../assets/css/style.css">

</head>

<body>

<?php


session_start();
if (!empty($_SESSION['login'])) {

    echo "<h1><center>Bem Vindo! " . ucwords($_SESSION['login']) . " <br> Agende agora seu serviço. </br></center></h1>

<div class=\"flex-container\"><center>
    <form action=\"agendar.php\" method=\"POST\">
    
        <div> <label for=\"exampleInputEmail1\">INSIRA A DATA PRETENDIDA</label><br />
            <input type=\"date\" name=\"datarev\" id=\"datarev\"><br />
            
            <label for=\"exampleInputEmail1\">INSIRA O HORÁRIO</label><br />
            <input type=\"time\" name=\"hora\" id=\"hora\"><br />
                              
            <label for=\"exampleInputPassword1\">DESCRIÇÃO DO SERVIÇO</label><br />
            <textarea name=\"descricao\" cols=\"50\" rows=\"6\" placeholder='Qual serviço você deseja?'></textarea>

            <br /> <button type=\"submit\" value=\"Salvar\" id=\"salvar\" class=\"btn btn-primary\">Salvar</button><br><br>

        </div>
    </form>    
    </center>
</div>

";


} else {
    echo "<h1> <center>Por favor, efetue o <a href='index.php'> Login </a> ou <a href='./../register/index.php'> Cadastre-se </a>para agendar sua revisão! </center></h1>";
//header("location: localhost:1234/views/users/login/");
}

?>


</body>

</html>
