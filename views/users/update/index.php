<link rel="stylesheet" href="./../../../assets/css/bootstrap.css">
<link rel="stylesheet" href="./../../../assets/css/style.css">

<?php
include "./../../../main.php";
?>

<!DOCTYPE html>
<html lang="pt">
<head>

    <meta charset="UTF-8">
    <title>ImperaMotos</title>

    <style>

        body {
            margin: 0px;
            padding: 0px;
            background-color: lightskyblue;
            background-image: url(./../../../images/1.jpeg);
            background-repeat: no-repeat;
            background-position: top;
        }

        .flex-container {
            padding: 50px;
            display: flex;
            justify-content: center;

        }

        @media screen and (min-width: 480px) {
            body {
                background-color: lightskyblue;
            }
        }

    </style>
</head>

<body>

<div class="flex-container">
    <form method="post" action="update.php">

        <div><label for="exampleInputEmail1">Endereço de Email</label><br/>
            <input type="text" name="login" id="login" aria-describedby="emailHelp"
                   placeholder="Insira o Email"><br/>

            <label for="exampleInputPassword1">Senha</label><br/>
            <input type="password" name="senha" id="senha" placeholder="Senha"><br/>

            <br/>
            <button type="submit" value="entrar" id="entrar" class="btn btn-primary">Alterar Usuario</button>
        </div>
    </form>
</div>

</body>
</html>

