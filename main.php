<?php
session_start();

if (!empty($_SESSION['login'])) {

    echo "

<nav class=\"navbar navbar-default\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"#\">ImperaMotos</a>
        </div>
        <div id=\"navbar\" class=\"collapse navbar-collapse\">
            <ul class=\"nav navbar-nav\">
                <li><a href=\"./../../../home.php\">Página Inicial</a></li>
                <li><a href=\"./../../../views/pecas/index.php\">Peças</a></li>
                <li><a href=\"./../../../views/servicos/index.php\">Serviços</a></li>
            </ul>

            <ul class=\"nav navbar-nav navbar-right\">
                <li><a href=\"./../../../views/users/login/index.php\">Logar</a></li>
                <li><a href=\"./../../../views/users/register/index.php\">Registrar-se</a></li>
                <li><a href=\"./../../../views/users/login/Sair.php\">Sair</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
";
} else {
    echo "

<html>
<head>
    <title>ImperaMotos</title>
    <link rel=\"stylesheet\" href=\"assets/css/bootstrap.css\">
    <link rel=\"stylesheet\" href=\"assets/css/style.css\">
    <style>


    </style>

</head>
<body>

<nav class=\"navbar navbar-default\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"#\">ImperaMotos</a>
        </div>
        <div id=\"navbar\" class=\"collapse navbar-collapse\">
            <ul class=\"nav navbar-nav\">
                <li><a href=\"./../../../home.php\">Página Inicial</a></li>
                <li><a href=\"./../../../views/pecas/index.php\">Peças</a></li>
                <li><a href=\"./../../../views/servicos/index.php\">Serviços</a></li>
            </ul>

            <ul class=\"nav navbar-nav navbar-right\">
                <li><a href=\"./../../../views/users/login/index.php\">Logar</a></li>
                <li><a href=\"./../../../views/users/register/index.php\">Registrar-se</a></li>


            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div>

</div><!-- /.container -->
</body>
</html> 
    
    ";
}
?>

