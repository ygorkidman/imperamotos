<html>
<head>
    <title>ImperaMotos</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">

</head>
<body>



    <?php
    include "main.php";
    include "views/home/index.php";

    ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h3><center>Quem somos?</center></h3>
                <p>ImperaMotos uma oficina especializada em motos de várias marcas e modelos.
                Venha conferir nossos preços e serviços, ranqueada no top 10 de melhor atendimento e prestação de serviços
                da cidade, com uma estrutura ótima e profissionais com vários anos de experiência.</p>

            </div>
            <div class="col-sm-4">
                <h3><center>Nossa missão</center></h3>
                <p>Estar sempre pronta para atender de forma excelente nossos clientes. Nossos clientes em primeiro lugar.</p>

            </div>
            <div class="col-sm-4">
                <h3><center>Nossa visão</center></h3>
                <p>Permanecer construindo um bom relacionamento com nossos clintes, disponibilizando serviços de qualidade
                garantindo a confiança dos clientes. </p>

            </div>
        </div>
    </div>
</v>
</div><!-- /.container -->
</body>
</html>